package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.lili.strava.modeles.ExerciceCardio;
import com.example.lili.strava.R;

import java.util.ArrayList;
import java.util.List;

public class ExerciceCardioActivity extends AppCompatActivity  {

    EditText idName;
    EditText idDistance;
    EditText idVitesse;
    Spinner spinnerExerciceCardio;
    Button btnAjouter;
    //ArrayList<ExerciceCardio> mlist;
  //  ExerciceAdapter adapter;
    //LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice_cardio);
        List<String> categories = new ArrayList<String>();
        categories.add("Choisissez Un Exercice");
        categories.add("Course");
        categories.add("Natation");

        spinnerExerciceCardio=(Spinner)findViewById(R.id.spinnerExerciceCardio);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerExerciceCardio.setAdapter(dataAdapter);

        btnAjouter=(Button)findViewById(R.id.btnAjouter);
        btnAjouter.setOnClickListener(submitAjouter);
    }

    private View.OnClickListener submitAjouter=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            idName=(EditText)findViewById(R.id.idName);
            idDistance=(EditText)findViewById(R.id.idDistance);
            idVitesse=(EditText)findViewById(R.id.idVitesse);


            String Name=idName.getText().toString();
            int Distance=Integer.parseInt(idDistance.getText().toString());
            int Vitesse=Integer.parseInt(idVitesse.getText().toString());
            spinnerExerciceCardio.getSelectedItem().toString();

            ExerciceCardio cardio=new ExerciceCardio(Name,Distance,Vitesse);
            System.out.println(cardio.ToJson().toString());
            //Modifier ladresse
            /*
            new HttpAsyncTask().execute("http://192.168.14.240:8080/JustRunIt_Application/api/utilisateur/create",
                    "POST",
                    cardio.toString());

*/

            Intent i = new Intent(v.getContext(), Programme.class);

            startActivity(i);


        }

    };
}
