package com.example.lili.strava.controleur;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lili.strava.modeles.ExerciceMusculation;
import com.example.lili.strava.R;

import java.util.ArrayList;

/**
 * Created by lili on 12/12/2017.
 */

public class exerciceMusculaireAdapter extends BaseAdapter {

    private ArrayList<ExerciceMusculation> exercicemMusculation;
    Context context;

    public exerciceMusculaireAdapter(ArrayList exercicemMusculation, Context context) {
        this.exercicemMusculation = exercicemMusculation;
        this.context = context;
    }

    @Override
    public int getCount() {
        return exercicemMusculation.size();
    }

    @Override
    public Object getItem(int position) {

        return exercicemMusculation.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<ExerciceMusculation> getAlbums() {
        return this.exercicemMusculation;
    }

    public void add(ExerciceMusculation per) {
        exercicemMusculation.add(per);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LinearLayout linearLayout = null;
        if (linearLayout != null) {
            linearLayout = (LinearLayout) convertView;
        } else {
            linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.row_exercice_musculaire, parent, false);
        }
        ExerciceMusculation currentsAlbum = (ExerciceMusculation) getItem(position);
        TextView idNom = (TextView) linearLayout.findViewById(R.id.idNom);
        TextView idNR = (TextView) linearLayout.findViewById(R.id.idNR);
        TextView idNS = (TextView) linearLayout.findViewById(R.id.idNS);
        TextView idTP = (TextView) linearLayout.findViewById(R.id.idTP);


        idNom.setText(currentsAlbum.getNomExercice());
        idNR.setText(currentsAlbum.getNombreRepetitions());
        idNS.setText(currentsAlbum.getNombreSeries());
        idTP.setText(currentsAlbum.getTempsPause());





        return linearLayout;
    }
}
