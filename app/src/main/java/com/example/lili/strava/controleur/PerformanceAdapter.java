package com.example.lili.strava.controleur;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lili.strava.modeles.Performance;
import com.example.lili.strava.R;

import java.util.ArrayList;

/**
 * Created by lili on 08/12/2017.
 */

public class PerformanceAdapter extends BaseAdapter {
    private ArrayList<Performance> performance;
    Context context;

    public PerformanceAdapter(ArrayList performance, Context context) {
        this.performance = performance;
        this.context = context;
    }

    @Override
    public int getCount() {
        return performance.size();
    }

    @Override
    public Object getItem(int position) {
        return performance.get(position);
    }

    public ArrayList<Performance> getAlbums(){
        return this.performance;

    }
    public void add(Performance per){
        performance.add(per);
        notifyDataSetChanged();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout linearLayout=null;
        if(linearLayout !=null){
            linearLayout=(LinearLayout)convertView;
        }else{
            linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.row_adapter, parent, false);
        }
        Performance currentsAlbum = (Performance)getItem(position);
        TextView textViewTitre = (TextView) linearLayout.findViewById(R.id.textTitre);
        TextView textViewLieu = (TextView) linearLayout.findViewById(R.id.textLieu);
        TextView textViewDate = (TextView) linearLayout.findViewById(R.id.textDate);
        TextView  idViewExercice =(TextView) linearLayout.findViewById(R.id.textExercice);
        TextView textViewKcal = (TextView) linearLayout.findViewById(R.id.Kcal);

        textViewTitre.setText(currentsAlbum.getTitre());
        textViewLieu.setText(currentsAlbum.getLieu());
        textViewDate.setText(currentsAlbum.getDate());
        idViewExercice.setText(currentsAlbum.getExercice().toString());
        textViewKcal.setText(Integer.toString(currentsAlbum.getCalories()));



        return linearLayout;
    }
}
