package com.example.lili.strava.modeles;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lili on 08/12/2017.
 */

public class Performance {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String titre;
    private String lieu;
    private String date;
    private int calories;
    private IExecutable exercice;

    private Utilisateur utilisateur;

    public Performance(String titre,
                       String lieu,
                       String date,
                       IExecutable exercice,
                       int calories
    ) {
        this.titre = titre;
        this.lieu = lieu;
        this.date = date;
        this.exercice = exercice;
        this.calories = calories;

    }

    public Performance(String titre,
                       String lieu,
                       String date,
                       int calories) {
        this.titre = titre;
        this.lieu = lieu;
        this.date = date;
        this.calories = calories;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setLieu(String lieu) {
        this.lieu = lieu;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public void setExercice(IExecutable exercice) {
        this.exercice = exercice;
    }

    public String getTitre() {
        return titre;
    }

    public String getLieu() {
        return lieu;
    }

    public String getDate() {
        return date;
    }

    public int getCalories() {
        return calories;
    }

    public IExecutable getExercice() {
        return exercice;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JSONObject ToJson() {
        JSONObject object = new JSONObject();

        try {
            object.put("Titre", titre);
            object.put("Lieu", lieu);
            object.put("date", date);
            object.put("Kcal", calories);
            object.put("Exercice", exercice);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}


