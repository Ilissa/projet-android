package com.example.lili.strava.controleur;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.lili.strava.http.AsyncResponse;
import com.example.lili.strava.http.HttpAsyncTask;
import com.example.lili.strava.modeles.Utilisateur;
import com.example.lili.strava.R;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class Formulaire extends AppCompatActivity {

    private EditText idName;
    private EditText IdFirstName;
    private EditText idDate;
    private EditText IdEmailFormulaire;
    private EditText IdMDPformulaire;
    private EditText IdPoint;
    private EditText IdTaille;
    private Button btnTerminer;
    private Button btnPrecedent;
    private RadioButton rbFemme;
    private RadioButton rbHomme;
    private TextView messageErrorName;
    private TextView messageErrorPrenom;
    private TextView messageErrorDate;
    private TextView messageErrorEmail;
    private TextView messageErrorMDP;
    private TextView messageErrorPoint;
    private TextView messageErrorTaille;
    private Utilisateur.Genre genre;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulaire);
        btnTerminer = (Button) findViewById(R.id.btnSuivant);
        btnTerminer.setOnClickListener(ClickTerminer);

        btnPrecedent = (Button) findViewById(R.id.btnPrecedent);
        btnPrecedent.setOnClickListener(ClickPrecedent);

        rbFemme = (RadioButton) findViewById(R.id.rbFemme);
        rbFemme.setOnClickListener(Checkrb);
        rbHomme = (RadioButton) findViewById(R.id.rbHomme);
        rbHomme.setOnClickListener(Checkrb);

    }

    private View.OnClickListener Checkrb = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onRadioButtonClicked(v);
        }


        public void onRadioButtonClicked(View view) {
            // Is the button now checked?
            boolean checked = ((RadioButton) view).isChecked();

            switch (view.getId()) {
                case R.id.rbFemme:
                    if (checked)
                        rbHomme.setChecked(!checked);
                       genre= Utilisateur.Genre.F;

                    break;
                case R.id.rbHomme:
                    if (checked)
                        rbFemme.setChecked(!checked);
                    genre= Utilisateur.Genre.H;

                    break;
            }
        }

        ;
    };
    private View.OnClickListener ClickTerminer = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            idName = (EditText) findViewById(R.id.idName);
            IdFirstName = (EditText) findViewById(R.id.IdFirstName);
            idDate = (EditText) findViewById(R.id.idDate);
            IdEmailFormulaire = (EditText) findViewById(R.id.IdEmailFormulaire);
            IdMDPformulaire = (EditText) findViewById(R.id.IdMDPformulaire);
            IdPoint = (EditText) findViewById(R.id.IdPoint);
            IdTaille = (EditText) findViewById(R.id.IdTaille);

            String name=idName.getText().toString();
            String prenom= IdFirstName.getText().toString();
            String date=idDate.getText().toString();
            String eMAIL=IdEmailFormulaire.getText().toString();
            String MDP=IdMDPformulaire.getText().toString();
            String taille=IdTaille.getText().toString();
            String poids=IdPoint.getText().toString();


            TerminerConnexion(name,prenom,eMAIL,MDP,date,taille,poids,v);

            //persister dans la base de donnée.
        }
    };
    private View.OnClickListener ClickPrecedent = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(v.getContext(), MainStrava.class);
            startActivity(i);
        }
    };

    private void TerminerConnexion(String name, String firstName, String Email,
                                   String mdp, String date, String taille,
                                   String piont, View v) {
        if (name.equals("")) {
            messageErrorName = (TextView) findViewById(R.id.messageErrorName);
            messageErrorName.setText("Champs Obligatoire");

        } else if (firstName.equals("")) {
            messageErrorPrenom = (TextView) findViewById(R.id.messageErrorPrenom);
            messageErrorPrenom.setText("Champs Obligatoire");

        } else if (date.equals("")) {
            messageErrorDate = (TextView) findViewById(R.id.messageErrorDate);
            messageErrorDate.setText("Champs Obligatoire");

        } else if (Email.equals("")) {
            messageErrorEmail = (TextView) findViewById(R.id.messageErrorEmail);
            messageErrorEmail.setText("Champs Obligatoire");

        } else if (mdp.equals("")) {
            messageErrorMDP = (TextView) findViewById(R.id.messageErrorMDP);
            messageErrorMDP.setText("Champs Obligatoire");

        } else if (piont.equals("")) {
            messageErrorPoint = (TextView) findViewById(R.id.messageErrorPoint);
            messageErrorPoint.setText("Champs Obligatoire");

        } else if (taille.equals("")) {
            messageErrorTaille = (TextView) findViewById(R.id.messageErrorTaille);
            messageErrorTaille.setText("Champs Obligatoire");

        } else {
            System.out.println("UTILISATEUR NOM " + name);
            System.out.println("UTILISATEUR PRENOM " + firstName);
            System.out.println("UTILISATEUR NAISSANCE " + date);
            System.out.println("UTILISATEUR EMAIL " + Email);
            System.out.println("UTILISATEUR MDP " + mdp);
            System.out.println("UTILISATEUR GENRE " + genre.toString());
            System.out.println("UTILISATEUR POIDS " + piont);
            System.out.println("UTILISATEUR TAILLE " + taille);

            //Utilisateur object=new Utilisateur(name,firstName,date,Email,mdp,genre,Double.parseDouble(piont),Double.parseDouble(taille));
            JSONObject objectUser = new JSONObject();
            try {
                objectUser.put("nom", name);
                objectUser.put("prenom", firstName);
                objectUser.put("email", Email);
                objectUser.put("naissance", date);
                objectUser.put("motDePasse", mdp);
                objectUser.put("genre", genre);
                objectUser.put("poids", piont);
                objectUser.put("taille", taille);


            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent i = new Intent(getApplicationContext(), TableauBord.class);
            i.putExtra("Bonjour", "Bonjour " + name);
            startActivityForResult(i, 99);
           // System.out.println("MON UTILISATEUR" + object.toString());
           /* new HttpAsyncTask(responseAvailable).execute("http://192.168.15.123:8080/WebApplication1/api/utilisateur/create",
                    "POST",
                    objectUser.toString());

*/


        }


    }

    // Ici j'implemente un class anonyme qui implemente directement
    // l'interface AsyncResponse
   /* private AsyncResponse responseAvailable = new AsyncResponse() {

        // la méthode httpTaskFinished est appelée à partir du onPostResult de
        // l'async task
        @Override
        public void httpTaskFinished(String res) {
            if(res.isEmpty()){
                Log.d("DEBUG","UTILISATEUR NUL");
            }
            else
            {
                Gson gSonUser = new Gson();
                Utilisateur userCreated = new Utilisateur();
                userCreated = gSonUser.fromJson(res, Utilisateur.class);
                if(!userCreated.getNom().equals(""))
                {
                    SharedPreferences sharedpreferences;
                    sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    Intent i = new Intent(getApplicationContext(), TableauBord.class);
                    i.putExtra("Bonjour", "Bonjour " + userCreated.getPrenom());
                    startActivityForResult(i, 99);
                }
            }


        }
    };*/

}

