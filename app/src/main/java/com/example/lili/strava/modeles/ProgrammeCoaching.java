package com.example.lili.strava.modeles;

import java.util.ArrayList;

/**
 * Created by lili on 08/12/2017.
 */

public class ProgrammeCoaching {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String nomProgramme;
    ArrayList<IExecutable> programmeExercices;


    private Utilisateur utilisateur;


    public ProgrammeCoaching(String nomProgramme)
    {
        this.nomProgramme = nomProgramme;
        programmeExercices = new ArrayList<IExecutable>();
    }

    public void addExercice(IExecutable exerciceToDo)
    {
        programmeExercices.add(exerciceToDo);
    }

    public void removeExercice(IExecutable exerciceToDo)
    {
        programmeExercices.remove(exerciceToDo);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    @Override
    public String toString() {
        return "modeles.ProgrammeCoaching[ id=" + id + " ]";
    }

}


