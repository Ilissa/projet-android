package com.example.lili.strava.modeles;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by lili on 08/12/2017.
 */

public class Utilisateur {
    private static final long serialVersionUID = 1L;

    private long id;
    private String nom;
    private String prenom;
    private String email;
    private String naissance;
    private String motDePasse;
    private Genre genre;
    private double poids;
    private double taille;



    private Groupe groupe;


    private ArrayList<Performance> performances;


    private ProgrammeCoaching programmeExercices;

    public enum Genre{
        F,
        H
    }

    public Utilisateur() {
    }

    public Utilisateur(String nom,
                       String prenom,
                       String naissance,
                       String email,
                       String motDePasse,
                       Genre genre) {
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.email = email;
        this.motDePasse = motDePasse;
        this.genre = genre;
    }

    public Utilisateur(String nom,
                       String prenom,
                       String naissance,
                       String email,
                       String motDePasse,
                       Genre genre,
                       double poids,
                       double taille) {
        this.nom = nom;
        this.prenom = prenom;
        this.naissance = naissance;
        this.email = email;
        this.motDePasse = motDePasse;
        this.genre = genre;
        this.poids = poids;
        this.taille = taille;
    }


    public long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNaissance() {
        return naissance;
    }

    public String getMotDePasse() {
        return motDePasse;
    }

    public Genre getGenre() {
        return genre;
    }

    public double getPoids() {
        return poids;
    }

    public double getTaille() {
        return taille;
    }

    public Groupe getGroupe() {
        return groupe;
    }

    public double getIMC (double poids,
                          double taille)
    {
        return poids/(Math.pow(taille, 2));
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPoids(double poids) {
        this.poids = poids;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNaissance(String naissance) {
        this.naissance = naissance;
    }

    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public void setTaille(double taille) {
        this.taille = taille;
    }

    public void setGroupe(Groupe groupe) {
        this.groupe = groupe;
    }

    public void removeGroupe() {
        this.groupe = null;
    }

    public void setPerformances(ArrayList<Performance> performances) {
        this.performances = performances;
    }

    public void setProgrammeExercices(ProgrammeCoaching programmeExercices) {
        this.programmeExercices = programmeExercices;
    }

    public void addPerformance(Performance performance){
        performances.add(performance);
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Utilisateur)) {
            return false;
        }
        Utilisateur other = (Utilisateur) object;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modeles.Utilisateur[ id=" + id + " ]";
    }

    public JSONObject ToJson() {
        JSONObject object = new JSONObject();

        try {
            object.put("nom", nom);
            object.put("prenom", prenom);
            object.put("email", email);
            object.put("naissance", naissance);
            object.put("motDePasse", motDePasse);
            object.put("genre", genre);
            object.put("poids", poids);
            object.put("taille", taille);




        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

}


