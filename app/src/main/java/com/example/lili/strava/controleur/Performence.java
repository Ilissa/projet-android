package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.lili.strava.modeles.*;
import com.example.lili.strava.modeles.ExerciceCardio;
import com.example.lili.strava.R;

import java.util.ArrayList;
import java.util.List;

public class Performence extends AppCompatActivity {

    ArrayList<Performance> mlist;
    PerformanceAdapter adapter;
    EditText idTitre;
    EditText idLieu;
    EditText idDate;
    Spinner spinnerChoiceExercice;
    EditText idKcal;
    Button idAjouter;
    LinearLayout layoutPerformanceCreate;

    IExecutable ParDefaut=new ExerciceCardio("Choisir");
    IExecutable Exercice_Cardio=new ExerciceCardio("Exercice Cardio");
    IExecutable Exercice_Musculaire=new ExerciceMusculation("Exercice Musculaire");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performence);

        mlist=new ArrayList<Performance>();
        adapter= new  PerformanceAdapter(mlist, this);
        ListView listView=(ListView)findViewById(R.id.listView);
        listView.setAdapter(adapter);

        layoutPerformanceCreate=(LinearLayout) findViewById(R.id.layout_performance);
       // getLayoutInflater().inflate(R.layout.second_layout, layoutPerformanceCreate);


        List<IExecutable> categories = new ArrayList<IExecutable>();
        categories.add(ParDefaut);
        categories.add(Exercice_Cardio);
        categories.add(Exercice_Musculaire);



        spinnerChoiceExercice = (Spinner) findViewById((R.id.idExercice));
        ArrayAdapter<IExecutable> dataAdapter = new ArrayAdapter<IExecutable>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);



        spinnerChoiceExercice.setAdapter(dataAdapter);

     //  idAjouter=(Button)findViewById(R.id.idAjouter);
       // idAjouter.setOnClickListener(clickSubmit);
        /*
        spinnerChoiceExercice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                                       int pos, long row) {
                if (pos == 1) {

                    Intent i = new Intent(v.getContext(),ExerciceCardioActivity.class);
                    startActivity(i);

                } else {

                }


            }
           @Override
            public void onNothingSelected(AdapterView<?> parent) {

            };

        });
        */
        spinnerChoiceExercice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String exercice= spinnerChoiceExercice.getSelectedItem().toString();
                if (exercice.equals("")) {
                    return;
                }
                else
                {

                    if(exercice.equals(Exercice_Cardio.toString()))
                    {
                            startPerformance(PerformanceCardio.class);
                    /*    Performance per=new Performance(creeString, lieu, date,new ExerciceCardio("Exercice Cardio"),Integer.parseInt(kcal));
                        adapter.add(per);
                        System.out.println(per.ToJson().toString());*/
                    }
                    else if (exercice.equals(Exercice_Musculaire.toString())) {

                        startPerformance(PerformanceMuscu.class);
                       /* Performance per=new Performance(creeString, lieu, date,new ExerciceCardio("Exercice Musculaire"),Integer.parseInt(kcal));
                        adapter.add(per);
                        System.out.println(per.ToJson().toString());*/
                    }else{

                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });


    }

    private void startPerformance(Class classe)
    {
        Intent intent = new Intent(this, classe);
        startActivity(intent);
    }



   /* private View.OnClickListener clickSubmit=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //idTitre = (EditText) findViewById(R.id.idTitre);
         //   idLieu = (EditText) findViewById((R.id.idLieu));
            idDate = (EditText) findViewById((R.id.idDate));
            spinnerChoiceExercice = (Spinner) findViewById((R.id.idExercice));
       //     idKcal = (EditText) findViewById((R.id.idKcal));

            String creeString = idTitre.getText().toString();
            String lieu = idLieu.getText().toString();
            String date = idDate.getText().toString();
            String exercice= spinnerChoiceExercice.getSelectedItem().toString();
            String kcal = idKcal.getText().toString();
            if (creeString.equals("") || lieu.equals("")||date.equals("")||exercice.equals("")||kcal.equals("")  ) {
                return;
            }
            else
            {
                if(exercice.equals(Exercice_Cardio.toString()))
                {
                    Performance per=new Performance(creeString, lieu, date,new ExerciceCardio("Exercice Cardio"),Integer.parseInt(kcal));
                    adapter.add(per);
                    System.out.println(per.ToJson().toString());
                } else if (exercice.equals(Exercice_Musculaire.toString())) {
                    Performance per=new Performance(creeString, lieu, date,new ExerciceCardio("Exercice Musculaire"),Integer.parseInt(kcal));
                    adapter.add(per);
                    System.out.println(per.ToJson().toString());
                }else{

                }






            }
        }

    };

*/

    }