package com.example.lili.strava.modeles;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lili on 13/12/2017.
 */

public class Playlist {

    private int id;

    private String nom;


    private List<Musique> musiques = new ArrayList<>();

    public Playlist(String nom) {
        this.nom = nom;
        musiques = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMusiques(List<Musique> musiques) {
        this.musiques = musiques;
    }

    public Playlist()
    {
    }

    public void addMusique(Musique musique)
    {
        musiques.add(musique);
        musique.getPlaylists().add(this);
    }

    public void removeMusique(Musique musique) {
        musiques.remove(musique);
        musique.getPlaylists().remove(this);

    }

    public int getDuree() {
        int duree = 0;
        for (Musique m : musiques) {
            if (m == null) continue;
            duree += m.getDuree();
        }
        return duree;
    }

    public int getId() {
        return id;
    }

    public List<Musique> getMusiques() {
        return musiques;
    }


}
