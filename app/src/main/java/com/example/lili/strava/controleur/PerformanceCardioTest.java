package com.example.lili.strava.controleur;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.lili.strava.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class PerformanceCardioTest extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;

    Location performanceCurrentLocation;
    Location performanceLastLocation;
    //private int timer_refreshCount = 5000;

    Marker mCurrLocationMarker;
    ArrayList<Marker> performanceMarkersList;
    LocationRequest mLocationRequest;


    //Performance data
    ArrayList<ArrayList> performanceDataDetails;

    Date performanceInitDate;
    Date performancelastDate;
    //Date performanceCurrentDate;
    long performanceGlobalTime;
    float performanceGlobalDistance;
    float performanceGlobalSpeed;

    //Performance View components
    LinearLayout l_performanceDetails;
    LinearLayout l_performanceGmap;
    LinearLayout l_performanceChrono;
    LinearLayout l_performanceDisplay;
    LinearLayout l_performanceGMapPicture;
    LinearLayout l_performanceRecord;
    LinearLayout l_performanceSave;

    LinearLayout l_performanceTitre;
    LinearLayout l_performanceLieu;
    LinearLayout l_performanceDuree;
    LinearLayout l_performanceDistance;
    LinearLayout l_performanceVitesse;
    LinearLayout l_performanceCalories;

    EditText txtDistance;
    EditText txtDuree;
    EditText txtVitesse;
    EditText txtCalories;
    EditText txtLieu;
    EditText txtTitre;
    Chronometer chronoPerformance;

    Button btn_startPerformance;
    Button btn_stopPerformance;
    Button btn_savePerformance;

    ImageView image_viewGmap;
    Boolean test = false;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_just_run_it_reboot_v2);
        Intent intent = getIntent();

        txtDistance = (EditText) findViewById(R.id.editDistance);
        txtDuree = (EditText) findViewById(R.id.editDuree);
        txtVitesse = (EditText) findViewById(R.id.editVitesse);
        txtCalories = (EditText) findViewById(R.id.editCalories);
        txtLieu = (EditText) findViewById(R.id.editLieu);
        txtTitre = (EditText) findViewById(R.id.editTitrePerformance);

        btn_startPerformance = (Button) findViewById(R.id.btn_Start);
        btn_startPerformance.setOnClickListener(listenerStart);
        btn_stopPerformance = (Button) findViewById(R.id.btn_Stop);
        btn_stopPerformance.setOnClickListener(listenerStop);
        btn_savePerformance = (Button) findViewById(R.id.btn_Save);
        btn_savePerformance.setOnClickListener(listenerSave);

        l_performanceDetails= (LinearLayout) findViewById(R.id.layout_PerformanceTitre);
       l_performanceGMapPicture = (LinearLayout) findViewById(R.id.layout_PerformanceGmapPicture);
        l_performanceGmap =(LinearLayout) findViewById(R.id.layout_PerformanceGmap);
        l_performanceChrono=(LinearLayout) findViewById(R.id.layout_Chrono);

        l_performanceTitre= (LinearLayout) findViewById(R.id.layout_titrePerformance);
        l_performanceLieu= (LinearLayout) findViewById(R.id.layout_lieuPerformance);
        l_performanceDuree= (LinearLayout) findViewById(R.id.layout_dureePerformance);
        l_performanceDistance= (LinearLayout) findViewById(R.id.layout_distancePerformance);
        l_performanceVitesse= (LinearLayout) findViewById(R.id.layout_vitessePerformance);
        l_performanceCalories= (LinearLayout) findViewById(R.id.layout_caloriesPerformance);

        chronoPerformance = (Chronometer) findViewById(R.id.chrono_performance);
        image_viewGmap = (ImageView) findViewById(R.id.imv_Gmap);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initMyPerformanceView();
    }

    //checks if the time between two refreshing point is elpased
    private boolean IsTimeRefreshElapsed() {

        if (performancelastDate != null) {
            Date performanceCurrentDate = new Date();
            long diff = TimeUnit.MILLISECONDS.toSeconds(performanceCurrentDate.getTime() - performancelastDate.getTime());
            return ((diff > 0) ? true : false);
            // return true;
        } else
            return false;

    }


    //returns the distance between two locations
    private float getDistance(Location begin, Location end) {
        return begin.distanceTo(end);
    }

    //returns the time elapsed between two dates
    private float getTimeElapsed() {
        Date performanceCurrentDate = new Date();
        long diff = TimeUnit.MILLISECONDS.toSeconds(performanceCurrentDate.getTime()
                - performancelastDate.getTime());
        return (float) diff;
    }

    //returns the velocity
    private long getVelocity(float distance, float time) {
        return (long) ((distance / 1000) / (time / 3600));
    }

    private String getLocationCity(Location locationCity) throws IOException {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = gcd.getFromLocation(locationCity.getLatitude(),
                locationCity.getLongitude(),
                1);
        if (addresses.size() > 0) {
            System.out.println(addresses.get(0).getLocality());
            return addresses.get(0).getLocality();
        } else {
            System.out.println("My City");
            return "My City";
        }

    }

    private View.OnClickListener listenerStart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            test = true;
            startMyPerformanceView();
            performanceDataDetails = new ArrayList<ArrayList>();
            performanceMarkersList = new ArrayList<Marker>();
            performanceGlobalTime = 0;
            performanceGlobalDistance = 0;
            performanceGlobalSpeed = 0;
            performancelastDate = new Date();
            performanceLastLocation = null;
            chronoPerformance.setBase(SystemClock.elapsedRealtime());
            chronoPerformance.start();
            startLocationsUpdate();
           // startTimer();

        }


    };

   /* private void startTimer()
    {
        new Timer().scheduleAtFixedRate(new TimerTask(){
            @Override
            public void run(){
                Log.i("tag", "5 seconds");
                float localTimePerformance =   timer_refreshCount;
                float localDistancePerformance = getDistance(performanceCurrentLocation, performanceLastLocation);
                float localVelocityPerformance = getVelocity(localDistancePerformance, localTimePerformance);
                long localCaloriesPerformance = 33;

                ArrayList localDetailsPerformance = new ArrayList();
                //localDetailsPerformance.add(localDistancePerformance);
                //localDetailsPerformance.add(localTimePerformance);
                //performanceDataDetails.add(localDetailsPerformance);

                performanceGlobalTime += localTimePerformance;
                performanceGlobalDistance += localDistancePerformance;
                //draw my local itinerary
                drawLocalItinerary(performanceCurrentLocation, performanceLastLocation);
                //refresh my view performance details
                refreshMyPerformanceView();

                //update my globals data for next refresh
                performanceLastLocation = performanceCurrentLocation;
                performancelastDate = new Date();;
            }
        },0,timer_refreshCount);
    }*/

    private View.OnClickListener listenerStop = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            performanceGlobalTime = (SystemClock.elapsedRealtime() - chronoPerformance.getBase())/1000;
            performanceGlobalSpeed = getVelocity(performanceGlobalDistance, performanceGlobalTime);
            chronoPerformance.setBase(SystemClock.elapsedRealtime());
            chronoPerformance.stop();
            stopLocationsUpdate();
            autoScalePerformanceVisualForSnapshot();
            takeSnapshot();

        }


    };

    private View.OnClickListener listenerSave = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }

    };

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (performanceLastLocation == null) {
            performanceLastLocation = location;
        }

        performanceCurrentLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        if(performanceMarkersList!=null)
        {
            performanceMarkersList.add(mCurrLocationMarker);
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker());
        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker());
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(50));

       // if (IsTimeRefreshElapsed())
        if (test ==true)
        {


     //   {
            //get my local performance datas

         //   float localTimePerformance = getTimeElapsed();
            long localTimePerformance = chronoPerformance.getDrawingTime();
            float localDistancePerformance = getDistance(performanceCurrentLocation, performanceLastLocation);
            //float localVelocityPerformance = getVelocity(localDistancePerformance, localTimePerformance);
            //long localCaloriesPerformance = 33;

            ArrayList localDetailsPerformance = new ArrayList();
            //localDetailsPerformance.add(localDistancePerformance);
            //localDetailsPerformance.add(localTimePerformance);
            //performanceDataDetails.add(localDetailsPerformance);

            //performanceGlobalTime += localTimePerformance;
            performanceGlobalDistance += localDistancePerformance;
            //draw my local itinerary
            drawLocalItinerary(performanceCurrentLocation, performanceLastLocation);
            //refresh my view performance details
            refreshMyPerformanceView();

            //update my globals data for next refresh
            performanceLastLocation = performanceCurrentLocation;
            performancelastDate = new Date();
     //   }
        }

    }



    private void stopLocationsUpdate() {
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }

    }

    private void startLocationsUpdate() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

        private void refreshMyPerformanceView()
        {
                txtDistance.setText(performanceGlobalDistance + "m");
                //txtVitesse.setText(velocity +"km/h");
                //txtCalories.setText(calories + "kcal");
        }

        private void initMyPerformanceView()
        {
                btn_startPerformance.setEnabled(true);
                btn_startPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnEnabled));
                btn_stopPerformance.setEnabled(false);
                btn_stopPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnDisabled));
                btn_savePerformance.setEnabled(false);
                btn_savePerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnDisabled));

                txtDuree.setEnabled(false);
                txtDuree.setText("0s");
                txtDistance.setEnabled(false);
                txtDistance.setText("0m");
                txtVitesse.setEnabled(false);
                txtVitesse.setText("0km/s");
                txtCalories.setEnabled(false);
                txtCalories.setText("0kcal");

            l_performanceTitre.setVisibility(LinearLayout.GONE);
            l_performanceLieu.setVisibility(LinearLayout.GONE);
            l_performanceDuree.setVisibility(LinearLayout.GONE);
            l_performanceVitesse.setVisibility(LinearLayout.GONE);
            l_performanceCalories.setVisibility(LinearLayout.GONE);

                l_performanceGMapPicture.setVisibility(LinearLayout.GONE);
            l_performanceDetails.setVisibility(LinearLayout.GONE);
            l_performanceChrono.setVisibility(LinearLayout.VISIBLE);
        }

    private void startMyPerformanceView()
    {
        btn_startPerformance.setEnabled(false);
        btn_startPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnDisabled));
        btn_stopPerformance.setEnabled(true);
        btn_stopPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnEnabled));
        btn_savePerformance.setEnabled(false);
        btn_savePerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnDisabled));
        l_performanceGmap.setVisibility(LinearLayout.VISIBLE);
        l_performanceGMapPicture.setVisibility(LinearLayout.GONE);
        l_performanceDetails.setVisibility(LinearLayout.GONE);
        l_performanceChrono.setVisibility(LinearLayout.VISIBLE);

        txtTitre.setEnabled(false);
        txtTitre.setText("Ma Performance");
        txtLieu.setEnabled(false);
        txtLieu.setText("Ma Ville");
        txtDuree.setEnabled(false);
        txtDuree.setText("0s");
        txtDistance.setEnabled(false);
        txtDistance.setText("0m");
        txtVitesse.setEnabled(false);
        txtVitesse.setText("0km/s");
        txtCalories.setEnabled(false);
        txtCalories.setText("0kcal");

        l_performanceTitre.setVisibility(LinearLayout.GONE);
        l_performanceLieu.setVisibility(LinearLayout.GONE);
        l_performanceDuree.setVisibility(LinearLayout.GONE);
        l_performanceVitesse.setVisibility(LinearLayout.GONE);
        l_performanceCalories.setVisibility(LinearLayout.GONE);
    }


    private void stopMyPerformanceView() throws IOException {
        btn_startPerformance.setEnabled(true);
        btn_startPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnEnabled));
        btn_stopPerformance.setEnabled(false);
        btn_stopPerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnDisabled));
        btn_savePerformance.setEnabled(true);
        btn_savePerformance.setBackgroundColor(getResources().getColor(R.color.colorBtnEnabled));

        l_performanceGmap.setVisibility(LinearLayout.GONE);
        l_performanceChrono.setVisibility(LinearLayout.GONE);
        l_performanceGMapPicture.setVisibility(LinearLayout.VISIBLE);
        l_performanceDetails.setVisibility(LinearLayout.VISIBLE);

        l_performanceTitre.setVisibility(LinearLayout.VISIBLE);
        l_performanceLieu.setVisibility(LinearLayout.VISIBLE);
        l_performanceDuree.setVisibility(LinearLayout.VISIBLE);
        l_performanceVitesse.setVisibility(LinearLayout.VISIBLE);
        l_performanceCalories.setVisibility(LinearLayout.VISIBLE);

        txtTitre.setEnabled(true);
        txtTitre.setText("Ma Performance");
        txtLieu.setEnabled(false);
        try {

            txtLieu.setText(getLocationCity(performanceCurrentLocation));
        } catch (IOException e) {
            e.printStackTrace();
        }
        txtDuree.setEnabled(false);
        txtDuree.setText(performanceGlobalTime + "s");
        txtDistance.setEnabled(false);
        txtDistance.setText(performanceGlobalDistance + "m");
        txtVitesse.setEnabled(false);
        txtVitesse.setText(getVelocity(performanceGlobalDistance,performanceGlobalTime)+ "km/h");
        txtCalories.setEnabled(false);
        txtCalories.setText("66kcal");
    }

        //Draws Line beatween two locations
        private void drawLocalItinerary(Location begin, Location end)
        {
                Polyline line = mMap.addPolyline(new PolylineOptions()
                        .add(new LatLng(begin.getLatitude(), begin.getLongitude()), new LatLng(end.getLatitude(), end.getLongitude()))
                        .width(5)
                        .color(Color.GREEN));
        }


    @Override
public void onConnectionFailed(ConnectionResult connectionResult) {


        }

public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
public boolean checkLocationPermission(){
        if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION)
        != PackageManager.PERMISSION_GRANTED) {

        // Asking user if explanation is needed
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
        Manifest.permission.ACCESS_FINE_LOCATION)) {

        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.

        //Prompt the user once explanation has been shown
        ActivityCompat.requestPermissions(this,
        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
        MY_PERMISSIONS_REQUEST_LOCATION);


        } else {
        // No explanation needed, we can request the permission.
        ActivityCompat.requestPermissions(this,
        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
        MY_PERMISSIONS_REQUEST_LOCATION);
        }
        return false;
        } else {
        return true;
        }
        }

@Override
public void onRequestPermissionsResult(int requestCode,
        String permissions[], int[] grantResults) {
        switch (requestCode) {
        case MY_PERMISSIONS_REQUEST_LOCATION: {
        // If request is cancelled, the result arrays are empty.
        if (grantResults.length > 0
        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        // permission was granted. Do the
        // contacts-related task you need to do.
        if (ContextCompat.checkSelfPermission(this,
        Manifest.permission.ACCESS_FINE_LOCATION)
        == PackageManager.PERMISSION_GRANTED) {

        if (mGoogleApiClient == null) {
        buildGoogleApiClient();
        }
        mMap.setMyLocationEnabled(true);
        }

        } else {

        // Permission denied, Disable the functionality that depends on this permission.
        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
        }
        return;
        }

        // other 'case' lines to check for other permissions this app might request.
        // You can add here other case statements according to your requirement.
        }
        }



    private void takeSnapshot() {
        if (mMap == null) {
            return;
        }

        final ImageView snapshotHolder = (ImageView) findViewById(R.id.imv_Gmap);

        final GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap snapshot) {
                // Callback is called from the main thread, so we can modify the ImageView safely.
                snapshotHolder.setImageBitmap(snapshot);
            }
        };


        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                try {
                    stopMyPerformanceView();
                } catch (IOException e) {
                    e.printStackTrace();
                }

               mMap.snapshot(callback);
            }
            });
        }

        private void autoScalePerformanceVisualForSnapshot()
        {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : performanceMarkersList) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int padding = 10; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.moveCamera(cu);
            mMap.animateCamera(cu);
        }
    }



