package com.example.lili.strava.http;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by lili on 08/12/2017.
 */

public class HttpAsyncTask extends AsyncTask<String, Void, String> {
    private AsyncResponse asyncResponse;

    public HttpAsyncTask() {
    }

    public HttpAsyncTask(AsyncResponse asyncResponse) {
        this.asyncResponse = asyncResponse;
    }
     @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    private void sendGet() {

    }


    private void sendPost(HttpURLConnection connection, String data) throws IOException {
        //Write data
        OutputStreamWriter osw =
                new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
        osw.write(data);
        osw.flush();
        osw.close();
    }


    @Override
    protected String doInBackground(String... params) {
        String url = params[0];
        String method = params[1];
        String data = params[2];
        System.out.println("url " + url);
        System.out.println("method " + method);
        System.out.println("Data " + params[2]);

// traitement faire une requete HTTP
// ...
        URL urlObj = null;
        HttpURLConnection connection = null;
        StringBuilder result = null;

        try {
            urlObj = new URL(url);
            connection = (HttpURLConnection) urlObj.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty("Content-Type", "application/json");
            if (method.equals("GET")) {
                this.sendGet();
            } else if (method.equals("POST")) {
                this.sendPost(connection, data);
            }
            connection.connect();

// code HTTP 200 404 500 ...
            int codeResponse = connection.getResponseCode();
            Log.d("CODE", "" + codeResponse);
            System.out.println("codeResponse");
            System.out.println(codeResponse);

            result = new StringBuilder();

            if (200 <= codeResponse && codeResponse < 300) {
// Je recupÃ¨re la rÃ©ponse de la requete
                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = null;

                while ((line = br.readLine()) != null) {
                    result.append(line); // ajoute la ligne de rÃ©ponse dans result
                }
                br.close();
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (result == null) {
            return "";
        }
        return result.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("Result = " + result);
        if (this.asyncResponse != null) {
            this.asyncResponse.httpTaskFinished(result);
        }


    }
}













