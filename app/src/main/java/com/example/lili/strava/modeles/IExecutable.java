package com.example.lili.strava.modeles;

/**
 * Created by lili on 08/12/2017.
 */

public interface IExecutable {
    public String faireExercice();
    public String toString();

}
