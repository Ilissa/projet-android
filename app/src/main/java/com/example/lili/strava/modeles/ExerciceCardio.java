package com.example.lili.strava.modeles;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by lili on 11/12/2017.
 */

public class ExerciceCardio extends Exercice implements IExecutable,Serializable {

    private Long id;
    static final private String TYPE_EXO = "Cardio";
    private int distanceExercice;
    private int vitesseExercice;

    public ExerciceCardio(String nomExercice) {
        super(nomExercice);
    }

    public ExerciceCardio(String nomExercice,
                          int distanceExercice,
                          int vitesseExercice)
    {
        super(nomExercice);
        this.distanceExercice = distanceExercice;
        this.vitesseExercice = vitesseExercice;
    }

    public Long getId() {
        return id;
    }



    public int getDistanceExercice() {
        return distanceExercice;
    }

    public int getVitesseExercice() {
        return vitesseExercice;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public void setDistanceExercice(int distanceExercice) {
        this.distanceExercice = distanceExercice;
    }

    public void setVitesseExercice(int vitesseExercice) {
        this.vitesseExercice = vitesseExercice;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExerciceCardio)) {
            return false;
        }
        ExerciceCardio other = (ExerciceCardio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String faireExercice() {
        return "Exercice : " + this.getNomExercice()+
                " /nType : " + TYPE_EXO +
                " /nDistance : " + distanceExercice +
                " /nVitesse : " + vitesseExercice +"";
    }

    @Override
    public String toString() {
        return ""+ this.getNomExercice() +"";
    }

    public JSONObject ToJson() {
        JSONObject object = new JSONObject();

        try {
            object.put("nomExercice", this.getNomExercice());
            object.put("distanceExercice", distanceExercice);
            object.put("vitesseExercice", vitesseExercice);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }
}

