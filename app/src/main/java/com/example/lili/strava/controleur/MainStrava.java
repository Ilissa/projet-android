package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.lili.strava.http.AsyncResponse;
import com.example.lili.strava.http.HttpAsyncTask;
import com.example.lili.strava.R;
import com.example.lili.strava.modeles.Utilisateur;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class MainStrava extends AppCompatActivity {

    private EditText IdEmail;
    private EditText IdMDP;
    private Button btnValider;
    private Button btnInscrire;
    private TextView MessageErrorEmail;
    private TextView MessageErrorMDP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_strava);
        btnValider = (Button) findViewById(R.id.btnValider);
        btnValider.setOnClickListener(ClickValider);

        btnInscrire = (Button) findViewById(R.id.btnInscrire);
        btnInscrire.setOnClickListener(ClickInscrire);

    }

    private View.OnClickListener ClickValider = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            IdEmail = (EditText) findViewById(R.id.IdEmail);
            IdMDP = (EditText) findViewById(R.id.IdMDP);

                /*
                chercher dans la base de donne
                 */
            ValiderConnexion(IdEmail.getText().toString(), IdMDP.getText().toString(), v);

        }
    };
    private View.OnClickListener ClickInscrire = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(v.getContext(), Formulaire.class);
            startActivity(i);

        }
    };

    private void ValiderConnexion(String Email, String mdp, View v) {
        if (Email.equals("")) {
            MessageErrorEmail = (TextView) findViewById(R.id.messgeErrorEmail);
            MessageErrorEmail.setText("Veuillez Entrer votre Email");
        }else if (mdp.equals("")) {
            MessageErrorMDP = (TextView) findViewById(R.id.messageErrorMDP);
            MessageErrorMDP.setText("Veuillez Entrer votre Mot de passe");

        } else {
            IdEmail = (EditText) findViewById(R.id.IdEmail);
            IdMDP = (EditText) findViewById(R.id.IdMDP);
            JSONObject object = new JSONObject();
            try {
                object.put("Email", IdEmail.getText().toString());
                object.put("MDP", IdMDP.getText().toString());

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Intent i = new Intent(v.getContext(), TableauBord.class);
            startActivity(i);
            System.out.println(object.toString());
            new HttpAsyncTask().execute("http://192.168.15.123:8080/JustRunIt_Application/api/utilisateur/connect",
                    "POST",
                    object.toString());
        }
    }


    // Ici j'implemente un class anonyme qui implemente directement
    // l'interface AsyncResponse
    private AsyncResponse responseAvailable = new AsyncResponse() {

        // la méthode httpTaskFinished est appelée à partir du onPostResult de
        // l'async task
        @Override
        public void httpTaskFinished(String res) {
            Gson gSonUser = new Gson();
            Utilisateur userCreated = new Utilisateur();
            userCreated = gSonUser.fromJson(res, Utilisateur.class);
            if(!userCreated.getNom().equals(""))
            {
                Intent i = new Intent(getApplicationContext(), TableauBord.class);
                i.putExtra("Bonjour", "Bonjour " + userCreated.getPrenom());
                startActivityForResult(i, 99);
            }

        }
    };

}



