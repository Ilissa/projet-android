package com.example.lili.strava.controleur;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lili.strava.modeles.*;
import com.example.lili.strava.R;

import java.util.ArrayList;

/**
 * Created by lili on 12/12/2017.
 */

public class ExerciceAdapter extends BaseAdapter {


    private ArrayList<Exercice> exercice;
    Context context;

    public ExerciceAdapter(ArrayList exercice, Context context) {
        this.exercice = exercice;
        this.context = context;
    }
    @Override
    public int getCount() {
        return exercice.size();
    }

    @Override
    public Exercice getItem(int position) {

        return exercice.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public ArrayList<Exercice> getAlbums() {
        return this.exercice;
    }

    public void add(Exercice per){
        exercice.add(per);
        notifyDataSetChanged();
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Exercice currentsAlbum = (Exercice) getItem(position);

        LinearLayout linearLayout = null;

        if (currentsAlbum instanceof ExerciceCardio) {
            if (linearLayout != null) {
                linearLayout = (LinearLayout) convertView;
            } else {
                linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.row_exercice_cardio, parent, false);
            }
            TextView idName = (TextView) linearLayout.findViewById(R.id.idName);
            TextView idDistanceCardio = (TextView) linearLayout.findViewById(R.id.idDistanceCardio);
            TextView idVitesseCardio = (TextView) linearLayout.findViewById(R.id.idVitesseCardio);

            idName.setText(currentsAlbum.getNomExercice());
            idDistanceCardio.setText(((ExerciceCardio) currentsAlbum).getDistanceExercice());
            idVitesseCardio.setText(((ExerciceCardio) currentsAlbum).getVitesseExercice());





            return linearLayout;
        }else if(currentsAlbum instanceof ExerciceMusculation){
            if (linearLayout != null) {
                linearLayout = (LinearLayout) convertView;
            } else {
                linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.row_exercice_musculaire, parent, false);
            }

            TextView idNom = (TextView) linearLayout.findViewById(R.id.idNom);
            TextView idNR = (TextView) linearLayout.findViewById(R.id.idNR);
            TextView idNS = (TextView) linearLayout.findViewById(R.id.idNS);
            TextView idTP = (TextView) linearLayout.findViewById(R.id.idTP);


            idNom.setText(currentsAlbum.getNomExercice());
            idNR.setText(((ExerciceMusculation) currentsAlbum).getNombreRepetitions());
            idNS.setText(((ExerciceMusculation) currentsAlbum).getNombreSeries());
            idTP.setText(((ExerciceMusculation) currentsAlbum).getTempsPause());





            return linearLayout;
        }
        return linearLayout;
        }
    }




