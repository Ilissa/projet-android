package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.lili.strava.modeles.ExerciceMusculation;
import com.example.lili.strava.R;

import java.util.ArrayList;
import java.util.List;

public class ExerciceMusculaireActivity extends AppCompatActivity {

    EditText idName;
    EditText nombreRepetitions;
    EditText nombreSeries;
    EditText idtempsPause;
    Button   BtnAjouter;

    Spinner spinnerMusco;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercice_musculaire);

        List<String> categories = new ArrayList<String>();
        categories.add("Choisissez Un Exercice");
        categories.add("Abdominaux ");
        categories.add("Pompes");
        categories.add("Tractions");
        spinnerMusco=(Spinner)findViewById(R.id.spinnerMusco) ;
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMusco.setAdapter(dataAdapter);
        BtnAjouter=(Button)findViewById(R.id.BtnAjouter);
        BtnAjouter.setOnClickListener(submitAjouter);

    }

    private View.OnClickListener submitAjouter=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            idName=(EditText)findViewById(R.id.idName);
            nombreRepetitions=(EditText)findViewById(R.id.nombreRepetitions);
            nombreSeries=(EditText)findViewById(R.id.nombreSeries);
            idtempsPause=(EditText)findViewById(R.id.idtempsPause);

            String nom=idName.getText().toString();
            int NR=Integer.parseInt(nombreRepetitions.getText().toString());
            int Ns=Integer.parseInt(nombreSeries.getText().toString());
            int TP=Integer.parseInt(idtempsPause.getText().toString());

            ExerciceMusculation ExoMu=new ExerciceMusculation(nom, NR, Ns, TP);


            System.out.println(ExoMu.ToJson().toString());

            //modifier ladresse
            /*
            new HttpAsyncTask().execute("http://192.168.14.240:8080/JustRunIt_Application/api/utilisateur/create",
                    "POST",
                    ExoMu.toString());
*/

            Intent i = new Intent(v.getContext(), Programme.class);
            startActivity(i);



        }
    };
}
