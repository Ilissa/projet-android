package com.example.lili.strava.controleur;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lili.strava.modeles.Musique;
import com.example.lili.strava.R;

    public class PlayListActivity extends AppCompatActivity {
        private ArrayList<Musique> songList;
        private ListView songView;
       private TextView IdListMusique;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_musique);
        songView = (ListView)findViewById(R.id.song_list);
        songList = new ArrayList<Musique>();


        Collections.sort(songList, new Comparator<Musique>(){
            @Override
            public int compare(Musique a, Musique b){

                return a.getTitre().compareTo(b.getTitre());
           }
        });
        SongAdapter songAdt = new SongAdapter(this, songList);
        songView.setAdapter(songAdt);

        Musique musique1=new Musique("Titre1","Article1");
        Musique musique2=new Musique("Titre2","Article2");
        Musique musique3=new Musique("Titre3","Article3");
        Musique musique4=new Musique("Titre4","Article4");
        Musique musique5=new Musique("Titre5","Article5");
        Musique musique6=new Musique("Titre6","Article6");

        songAdt.addMusique(musique1);
        songAdt.addMusique(musique2);
        songAdt.addMusique(musique3);
        songAdt.addMusique(musique4);
        songAdt.addMusique(musique5);
        songAdt.addMusique(musique6);

        songView.setOnItemClickListener(listPairedClickItem);

    }
        private AdapterView.OnItemClickListener listPairedClickItem = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

             System.out.println("Event list View");
                IdListMusique=(TextView)findViewById(R.id.IdListMusique);
                IdListMusique.setText(songList.get(arg2).getTitre());
            }
        };


}
