package com.example.lili.strava.http;

/**
 * Created by lili on 08/12/2017.
 */

public interface AsyncResponse {
    void httpTaskFinished(String s);
}
