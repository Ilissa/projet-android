package com.example.lili.strava.controleur;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lili.strava.modeles.Musique;
import com.example.lili.strava.R;

/**
 * Created by lili on 13/12/2017.
 */

public class SongAdapter extends BaseAdapter{

    private ArrayList<Musique> songs;

    private Context context;

    public SongAdapter(Context c, ArrayList<Musique> theSongs){
       songs=theSongs;
        this.context=c;

    }

    @Override
    public int getCount() {
        return songs.size();

    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void addMusique(Musique musique){
        songs.add(musique);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout songInf =null;

        if (songInf != null) {
            songInf = (LinearLayout) convertView;
        } else {
            songInf = (LinearLayout)LayoutInflater.from(context).inflate(R.layout.song, parent, false);
        }

        TextView songView = (TextView)songInf.findViewById(R.id.song_title);
        TextView artistView = (TextView)songInf.findViewById(R.id.song_artist);

        Musique currSong = (Musique)getItem(position);

        songView.setText(currSong.getTitre());
        artistView.setText(currSong.getArtiste());
     //   songInf.setTag(position);
        return songInf;
    }
}
