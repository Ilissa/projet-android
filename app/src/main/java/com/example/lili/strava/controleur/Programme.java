package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.lili.strava.modeles.Exercice;
import com.example.lili.strava.R;

import java.util.ArrayList;
import java.util.List;

public class Programme extends AppCompatActivity {

    TextView idProgrammeExercice;
    Spinner spinnerProgramme;
    ArrayList<Exercice>mlist;
    ExerciceAdapter Adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_programme);

        mlist=new ArrayList<Exercice>();
        ListView listView=(ListView)findViewById(R.id.listView);
        listView.setAdapter(Adapter);

        //recuperation des exercices dans la base et ajouter dans la list.






        spinnerProgramme=(Spinner)findViewById(R.id.spinnerProgramme);
        List<String> categories = new ArrayList<String>();
        categories.add("Choisissez un Exercice");
        categories.add("Exercice Cardio");
        categories.add("Exercice Musculaire");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerProgramme.setAdapter(dataAdapter);

        spinnerProgramme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View v,
                                       int pos, long row) {
                if (pos == 1) {

                    Intent i = new Intent(v.getContext(),ExerciceCardioActivity.class);
                    startActivity(i);

                } else if(pos == 2) {
                    Intent i = new Intent(v.getContext(),ExerciceMusculaireActivity.class);
                    startActivity(i);

                }else{

                }


            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            };

        });



        }


    }

