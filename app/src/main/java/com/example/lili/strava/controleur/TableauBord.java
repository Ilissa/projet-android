package com.example.lili.strava.controleur;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.lili.strava.R;

public class TableauBord extends AppCompatActivity {

    Button btn_ImagePerformence;
    Button btn_imageProgramme;
    Button btn_imageStatique;
    Button btn_imageMusique;
    Button btn_imageGroupe;
    Button btn_imageAmie;

    private TextView IdText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tableau_bord);
        String s=getIntent().getStringExtra("Bonjour");
        IdText=(TextView)findViewById(R.id.IdText);
        IdText.setText(s);

        btn_ImagePerformence=(Button)findViewById(R.id.button_performance);
        btn_imageProgramme=(Button)findViewById(R.id.button_programme);
        btn_imageStatique=(Button)findViewById(R.id.button_stat);
        btn_imageMusique=(Button)findViewById(R.id.button_music);
        btn_imageGroupe=(Button)findViewById(R.id.button_group);
        btn_imageAmie=(Button)findViewById(R.id.button_amis);

        btn_ImagePerformence.setOnClickListener(submitPerformance);
        btn_imageProgramme.setOnClickListener(submitProgramme);
        btn_imageStatique.setOnClickListener(submitStatique);
        btn_imageMusique.setOnClickListener(submitMusique);
        btn_imageGroupe.setOnClickListener(submitGroupe);
        btn_imageAmie.setOnClickListener(submitAmie);
    }
    private OnClickListener submitPerformance=new OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("Button Performance");
            Intent i = new Intent(v.getContext(), Performence.class);
            startActivity(i);

        }
    };

    private OnClickListener submitProgramme=new OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("Button programme");
            Intent i = new Intent(v.getContext(), Programme.class);
            startActivity(i);

        }
    };
    private OnClickListener submitStatique=new OnClickListener(){
        @Override
        public void onClick(View v) {

        }
    };
    private OnClickListener submitMusique=new OnClickListener(){
        @Override
        public void onClick(View v) {
            System.out.println("Button Musique");
            Intent i = new Intent(v.getContext(), PlayListActivity.class);
            startActivity(i);

        }
    };
    private OnClickListener submitGroupe=new OnClickListener(){
        @Override
        public void onClick(View v) {

        }
    };
    private OnClickListener submitAmie=new OnClickListener(){
        @Override
        public void onClick(View v) {

        }
    };





}
