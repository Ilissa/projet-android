package com.example.lili.strava.modeles;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lili on 11/12/2017.
 */

public class ExerciceMusculation extends Exercice implements IExecutable {
    private static final long serialVersionUID = 1L;

    private Long id;
    static final private String TYPE_EXO = "Musculation";

    private int nombreRepetitions;
    private int nombreSeries;
    private int tempsPause;

    public ExerciceMusculation(String nomExercice) {
        super(nomExercice);
    }

    public ExerciceMusculation(String nomExercice,
                               int nombreRepetitions,
                               int nombreSeries,
                               int tempsPause)
    {
        super(nomExercice);
        this.nombreRepetitions = nombreRepetitions;
        this.nombreSeries = nombreSeries;
        this.tempsPause = tempsPause;
    }

    public Long getId() {
        return id;
    }



    public int getNombreRepetitions() {
        return nombreRepetitions;
    }

    public int getNombreSeries() {
        return nombreSeries;
    }

    public int getTempsPause() {
        return tempsPause;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public void setNombreRepetitions(int nombreRepetitions) {
        this.nombreRepetitions = nombreRepetitions;
    }

    public void setNombreSeries(int nombreSeries) {
        this.nombreSeries = nombreSeries;
    }

    public void setTempsPause(int tempsPause) {
        this.tempsPause = tempsPause;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExerciceMusculation)) {
            return false;
        }
        ExerciceMusculation other = (ExerciceMusculation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

   @Override
    public String faireExercice() {
        return "Exercice : " + this.getNomExercice() +
                "/nType : " + TYPE_EXO +
                "/nNombre répétitions : " + nombreRepetitions +
                "/nNombre séries : " + nombreSeries +
                "/nTemps pause : " + tempsPause +"";
    }
    @Override
    public String toString() {
        return ""+ this.getNomExercice() +"";
    }

    public JSONObject ToJson() {
        JSONObject object = new JSONObject();

        try {
            object.put("nomExercice", this.getNomExercice());
            object.put("nombreRepetitions", nombreRepetitions);
            object.put("nombreSeries", nombreSeries);
            object.put("tempsPause", tempsPause);



        } catch (JSONException e) {
            e.printStackTrace();
        }
        return object;
    }

}
