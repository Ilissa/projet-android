package com.example.lili.strava.controleur;

import android.content.DialogInterface;
import android.os.SystemClock;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.lili.strava.R;
import com.example.lili.strava.modeles.IExecutable;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class PerformanceMuscu extends AppCompatActivity {

    Spinner spinnerChoiceExercice;
    EditText et_Titre;
    EditText et_NbRepet;
    EditText et_NbSeries;
    EditText et_TempsPause;
    ImageView imv_Anim;
    Button btnSave;

    LinearLayout layoutTitre;
    LinearLayout layoutChoix;
    LinearLayout layoutAnim;
    LinearLayout layoutDetails;
    LinearLayout layoutSave;

    GifImageView gigImvPompes;
    GifImageView gigImvTractions;
    GifImageView gigImvAbdos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_performance_muscu);

        List<String> categories = new ArrayList<String>();
        categories.add("Choisir");
        categories.add("Pompes");
        categories.add("Abdominaux");
        categories.add("Tractions");

        et_Titre= (EditText) findViewById((R.id.et_Titre));
        et_NbRepet= (EditText) findViewById((R.id.et_Repet));
        et_NbSeries= (EditText) findViewById((R.id.et_Series));
        et_TempsPause= (EditText) findViewById((R.id.et_Pause));


        gigImvPompes = (GifImageView)findViewById((R.id.imv_AnimPompes));
        gigImvAbdos = (GifImageView)findViewById((R.id.imv_AnimAbdos));
        gigImvTractions = (GifImageView)findViewById((R.id.imv_AnimTractions));

        btnSave = (Button) findViewById((R.id.btn_savePerformance));
        btnSave.setBackgroundColor(getResources().getColor(R.color.colorBtnEnabled));
        btnSave.setOnClickListener(listenerSave);
        btnSave.setVisibility(Button.GONE);

        layoutAnim = (LinearLayout) findViewById((R.id.layout_anim));

        spinnerChoiceExercice = (Spinner) findViewById((R.id.spinner_choiceExo));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerChoiceExercice.setAdapter(dataAdapter);





        spinnerChoiceExercice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String exercice= spinnerChoiceExercice.getSelectedItem().toString();
                if (exercice.equals("Choisir")) {
                    return;
                }
                else
                {
                    layoutAnim.setVisibility(LinearLayout.VISIBLE);
                    btnSave.setVisibility(Button.VISIBLE);
                    if(exercice.equals("Pompes"))
                    {
                        gigImvPompes.setVisibility(GifImageView.VISIBLE);
                        gigImvAbdos.setVisibility(GifImageView.GONE);
                        gigImvTractions.setVisibility(GifImageView.GONE);
                       // gigImvPompes.startAnimation();
                    }
                    else if(exercice.equals ("Abdominaux")) {
                        gigImvPompes.setVisibility(GifImageView.GONE);
                        gigImvAbdos.setVisibility(GifImageView.VISIBLE);
                        gigImvTractions.setVisibility(GifImageView.GONE);

                    }
                     else if (exercice.equals("Tractions")) {
                        gigImvPompes.setVisibility(GifImageView.GONE);
                        gigImvAbdos.setVisibility(GifImageView.GONE);
                        gigImvTractions.setVisibility(GifImageView.VISIBLE);

                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }




        });

    }

    private boolean isInvalidMyFields()
    {
        if(et_Titre.getText().equals(""))
        {
            return true;
        }
        if(et_NbRepet.getText().equals(""))
        {
            return true;
        }
        if(et_NbSeries.getText().equals(""))
        {
            return true;
        }
        if(et_TempsPause.getText().equals(""))
        {
            return true;
        }
        return false;
    }
    private View.OnClickListener listenerSave = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if(isInvalidMyFields()==true)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(PerformanceMuscu.this).create();
                alertDialog.setTitle("Saisie Incomplète");
                alertDialog.setMessage("Remplissez les champs demandés");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();

            }
            else
            {
                //TODO: saveMyPerformance();
            }

        }


    };

}
