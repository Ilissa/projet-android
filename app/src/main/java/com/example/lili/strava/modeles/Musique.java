package com.example.lili.strava.modeles;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lili on 13/12/2017.
 */

public class Musique {

    protected Long id;
    private String titre, artiste;
    private int duree;
    private final int QUALITE = 256;


    private List<Playlist> playlists=new ArrayList<>();

    public static void setNb(int nb) {
        Musique.nb = nb;
    }

    public List<Playlist> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<Playlist> playlists) {
        this.playlists = playlists;
    }


    public void setId(Long id) {
        this.id = id;
    }

    private static int nb = 0;


    public Long getId() {
        return id;
    }

    public int getQUALITE() {
        return QUALITE;
    }



    // Constructeurs
    public Musique(String unArtiste, String unTitre) {
        this(unArtiste, unTitre, 180);
    }

    public Musique(String unArtiste, String unTitre, int uneDuree) {
        artiste = unArtiste;
        titre = unTitre;
        duree = uneDuree;
        nb++;
    }

    public Musique()
    {

    }



    // Accesseur
    public String getArtiste() {
        return artiste;
    }

    public void setArtiste(String artiste) {
        this.artiste = artiste;
    }

    public String getTitre() {
        return this.titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getDuree() {
        return duree;
    }

    public static int getNb() {
        return nb;
    }

    // Modificateur
    public void setDuree(int sec) {
        if (sec >= 0) {
            duree = sec;
        } else {
            System.out.println("Erreur : la durée doit être positive");
        }
    }

    @Override
    public String toString() {
        return "Musique{" +
                "titre=" + titre +
                ", artiste=" + artiste +
                ", duree=" + duree +
                '}';
    }

}
