package com.example.lili.strava.modeles;

/**
 * Created by lili on 12/12/2017.
 */

public abstract class Exercice {

    private String NomExercice;

    public Exercice(String nomExercice) {
        NomExercice = nomExercice;
    }

    public String getNomExercice() {
        return NomExercice;
    }

    public void setNomExercice(String nomExercice) {
        NomExercice = nomExercice;
    }

    public abstract String toString();
}
